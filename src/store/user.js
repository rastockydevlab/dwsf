import { defineStore } from "pinia";

export const useUserStore = defineStore("user", {
  state: () => ({
    loggedIn: false,
    id: null,
    name: "",
    email: "",
    avatar: false,
    avatarUrl: "",
  }),
  actions: {
    setName(name) {
      this.name = name;
    },
    setEmail(email) {
      this.email = email;
    },
    setAvatar(avatar) {
      this.avatarUrl = avatar;
    },
    async logIn(credentials) {
      const { email, password } = credentials;
      const response = await fetch(
        `${
          import.meta.env.VITE_API_URL
        }/users?email=${email}&password=${password}`
      );

      const data = await response.json();
      if (data.length > 0) {
        this.loggedIn = true;
        this.id = data[0].id;
        this.name = data[0].name;
        this.email = data[0].email;

        if (data[0].avatar) {
          console.log("has avatar");
          this.avatar = false;
          this.avatarUrl = data[0].avatar;
        } else {
          this.avatar = true;
          this.avatarUrl = "";
        }
        return true;
      } else {
        return false;
      }
    },
    logOut() {
      this.loggedIn = false;
      this.id = null;
      this.name = "";
      this.email = "";
      this.avatar = false;
      this.avatarUrl = "";
    },
    async saveUserProfile() {
      const response = await fetch(
        `${import.meta.env.VITE_API_URL}/users/${this.id}`,
        {
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: this.name,
            email: this.email,
            avatar: this.avatarUrl,
          }),
        }
      );

      return response.ok;
    },
  },
  getters: {
    getName: (state) => state.name,
    getEmail: (state) => state.email,
    getAvatar: (state) => state.avatar,
    isLoggedIn: (state) => state.loggedIn,
  },
});
