import { defineStore } from "pinia";

export const useDateStore = defineStore("date", {
  state: () => ({
    today: new Date(),
    selectedDay: new Date().getDate(),
    selectedMonth: new Date().getMonth(),
    selectedYear: new Date().getFullYear(),
  }),
  actions: {
    selectDay(day) {
      this.selectedDay = day;
    },
  },
});
