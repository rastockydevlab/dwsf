// Utilities
import { defineStore } from "pinia";

const testAttendees = [
  {
    id: 1,
    name: "Matej Rástocký",
    avatar: "https://rastocky.sk/assets/profile.jpg",
  },
  {
    id: 2,
    name: "Onrej Kováč",
    avatar: "https://klubgerlach.sk/assets/img/ondrej_kovac.jpg",
  },
  {
    id: 3,
    name: "Ángel Diaz",
    avatar: "https://api.dicebear.com/6.x/identicon/svg?seed=Marek",
  },
  {
    id: 4,
    name: "Allan Bohm",
    avatar: "https://api.dicebear.com/6.x/identicon/svg?seed=Allan",
  },
];

const todaysEvents = [
  {
    id: 1,
    title: "Test Obed",
    start: new Date(2023, 7, 4, 13, 0),
    end: new Date(2023, 7, 4, 14, 0),
    location: "Dowina",
    locked: true,
    attendees: testAttendees,
    attending: true,
  },
  {
    id: 2,
    title: "Test Večera",
    start: new Date(2023, 7, 4, 20, 15),
    end: new Date(2023, 7, 4, 21, 30),
    location: "Dowina",
    locked: true,
    attendees: testAttendees,
    attending: false,
  },
];

const tomorrowsEvents = [
  {
    id: 3,
    title: "Test Obed",
    start: new Date(2023, 7, 3, 13, 0),
    end: new Date(2023, 7, 3, 14, 0),
    location: "Dowina",
    locked: true,
    attendees: testAttendees,
    attending: false,
  },
  {
    id: 4,
    title: "Test Večera",
    start: new Date(2023, 7, 4, 20, 15),
    end: new Date(2023, 7, 4, 21, 30),
    location: "Dowina",
    locked: false,
    attendees: testAttendees,
    attending: true,
  },
];

export const useAppStore = defineStore("app", {
  state: () => ({
    loading: false,
    events: new Map(),
  }),
  actions: {
    seedEvents() {
      this.events.set(todaysEvents[0].start.toDateString(), todaysEvents);
      this.events.set(tomorrowsEvents[0].start.toDateString(), tomorrowsEvents);
    },
    toggleAttending(event) {
      event.attending = !event.attending;

      // Make API call to update event, return success / error
    },
  },
  getters: {
    getEvents: (state) => (day) => {
      if (!state.events.has(day)) {
        state.seedEvents();
      }

      return state.events.get(day);
    },
  },
});
