// Composables
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    component: () => import("@/layouts/default/Default.vue"),
    children: [
      {
        path: "",
        name: "Home",
        component: () => import("@/views/HomeView.vue"),
        meta: {
          hideMenu: true,
        },
      },
    ],
  },
  {
    path: "/login",
    component: () => import("@/layouts/default/Default.vue"),
    children: [
      {
        path: "",
        name: "Login",
        component: () => import("@/views/LoginView.vue"),
        meta: {
          hideMenu: true,
        },
      },
    ],
  },
  {
    path: "/plan",
    component: () => import("@/layouts/default/Default.vue"),
    children: [
      {
        path: "",
        name: "Plan",
        component: () => import("@/views/PlanView.vue"),
        meta: {
          protected: true,
        },
      },
    ],
  },
  {
    path: "/calendar",
    component: () => import("@/layouts/default/Default.vue"),
    children: [
      {
        path: "",
        name: "Calendar",
        component: () => import("@/views/CalendarView.vue"),
        meta: {
          protected: true,
        },
      },
    ],
  },
  {
    path: "/profile",
    component: () => import("@/layouts/default/Default.vue"),
    children: [
      {
        path: "",
        name: "Profile",
        component: () => import("@/views/ProfileView.vue"),
        meta: {
          protected: true,
        },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

import { useUserStore } from "@/store/user";

router.beforeEach(async (to) => {
  const userStore = useUserStore();

  if (to.meta.protected && !userStore.isLoggedIn) {
    return { name: "Login" };
  } else {
    return true;
  }
});

export default router;
